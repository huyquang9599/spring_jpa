-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th3 22, 2020 lúc 09:06 AM
-- Phiên bản máy phục vụ: 10.4.11-MariaDB
-- Phiên bản PHP: 7.2.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `icecream`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `customer`
--

CREATE TABLE `customer` (
  `id` bigint(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `password` varchar(1024) NOT NULL,
  `DOB` date NOT NULL,
  `address` varchar(1024) NOT NULL,
  `gender` tinyint(1) NOT NULL DEFAULT 0,
  `avatar` varchar(2048) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `numofloginfailed` smallint(6) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `customer`
--

INSERT INTO `customer` (`id`, `name`, `email`, `phone`, `password`, `DOB`, `address`, `gender`, `avatar`, `status`, `numofloginfailed`) VALUES
(1, 'huy', 'huyquang9599@gmail.com', '09090909', '123', '1999-09-05', 'abc road', 1, NULL, 1, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `faq`
--

CREATE TABLE `faq` (
  `id` bigint(20) NOT NULL,
  `question` text NOT NULL,
  `answer` text NOT NULL,
  `status` smallint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `faq`
--

INSERT INTO `faq` (`id`, `question`, `answer`, `status`) VALUES
(1, 'Where can I get coupons for Parlor products?', 'Occasionally we offer coupons and samples on our website and Facebook page.', 0),
(2, 'Do your products contain allergens or gluten?', 'Some products do contain allergens, including gluten. However, most of our ice creams and frozen dairy desserts are naturally gluten free. We are currently updating our packaging to make it easy for you to identify all of our naturally gluten free variants. It is our policy that when any of the eight most common allergens (milk, eggs, fish, wheat, tree nuts, peanuts, soybeans and crustaceans) occur in any of our products they will be listed inside the ingredient statement in plain language.', 0),
(3, 'How Can I Buy Book Online?', 'You Should Login to The Member Page and Leave your Personal infomation there .Our deliver service base on this.', 0),
(4, 'Is my daughter safe on Parlor Icecream? ', 'User safety is our most important concern. Always Icecream is using a wide range of methods and processes to ensure user safety and privacy. The site is content is continuously scanned for inappropriate content using both software and manual reviews. Users can also report any concerns or problems they encounter to Always Icecream staff. Furthermore, self-policing mechanisms are in place to encourage proper behavior and etiquette. Lastly, parents receive regular email reports about the activities and learning progress of their daughter. ', 0),
(5, 'If my recipe is Prized what way i receive my prize money?', 'You can go direct to Parlor Shop or your banking...', 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `feedback`
--

CREATE TABLE `feedback` (
  `id` bigint(20) NOT NULL,
  `orderId` bigint(20) NOT NULL,
  `details` varchar(1024) NOT NULL,
  `createdDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `feedback`
--

INSERT INTO `feedback` (`id`, `orderId`, `details`, `createdDate`) VALUES
(1, 1, 'details', '2020-09-05');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(7);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `icecream`
--

CREATE TABLE `icecream` (
  `id` bigint(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(2048) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `icecream`
--

INSERT INTO `icecream` (`id`, `name`, `description`) VALUES
(1, 'name', 'description');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `orderdetail`
--

CREATE TABLE `orderdetail` (
  `orderId` bigint(20) NOT NULL,
  `recipeId` bigint(20) NOT NULL,
  `quantity` smallint(6) NOT NULL DEFAULT 1,
  `price` float NOT NULL,
  `notes` varchar(1024) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) NOT NULL,
  `customerId` bigint(20) NOT NULL,
  `paymentOption` varchar(100) NOT NULL,
  `paymentId` bigint(20) NOT NULL,
  `createDate` date NOT NULL,
  `deliveryDetail` varchar(1024) DEFAULT NULL,
  `notes` varchar(1024) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `orders`
--

INSERT INTO `orders` (`id`, `customerId`, `paymentOption`, `paymentId`, `createDate`, `deliveryDetail`, `notes`, `status`) VALUES
(1, 1, 'paymentOption', 1, '2020-03-05', 'deliveryDetail', 'notes', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `payment`
--

CREATE TABLE `payment` (
  `id` bigint(20) NOT NULL,
  `cardType` varchar(100) NOT NULL,
  `cardNumber` varchar(100) NOT NULL,
  `cvv` varchar(100) DEFAULT NULL,
  `nameOnCard` varchar(100) NOT NULL,
  `expiredDate` date NOT NULL,
  `DOB` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `payment`
--

INSERT INTO `payment` (`id`, `cardType`, `cardNumber`, `cvv`, `nameOnCard`, `expiredDate`, `DOB`) VALUES
(1, 'cardType', 'cardNumber', 'cvv', 'nameOnCard', '2020-08-07', '1999-09-05');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `recipe`
--

CREATE TABLE `recipe` (
  `id` bigint(20) NOT NULL,
  `userId` bigint(20) NOT NULL,
  `iceCreamId` bigint(20) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(2048) DEFAULT NULL,
  `price` float NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `viewCount` smallint(6) NOT NULL DEFAULT 0,
  `image` varchar(2048) DEFAULT NULL,
  `details` varchar(2048) NOT NULL,
  `uploadDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `recipe`
--

INSERT INTO `recipe` (`id`, `userId`, `iceCreamId`, `title`, `description`, `price`, `status`, `viewCount`, `image`, `details`, `uploadDate`) VALUES
(2, 1, 1, 'title', 'description', 70000, 1, 0, NULL, 'details', '2020-05-03');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `role`
--

CREATE TABLE `role` (
  `id` bigint(20) NOT NULL,
  `role` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `role`
--

INSERT INTO `role` (`id`, `role`) VALUES
(1, 'role3'),
(2, 'role3');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `password` varchar(1024) NOT NULL,
  `phoneNumber` varchar(50) NOT NULL,
  `avatar` varchar(2048) DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `user`
--

INSERT INTO `user` (`id`, `name`, `status`, `password`, `phoneNumber`, `avatar`, `phone_number`) VALUES
(1, 'name', 1, 'password', 'phoneNumber', 'avatar', '09909909');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `userrole`
--

CREATE TABLE `userrole` (
  `role_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK13hkc5jl898fls7w1fy1tl6pf` (`orderId`);

--
-- Chỉ mục cho bảng `icecream`
--
ALTER TABLE `icecream`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `orderdetail`
--
ALTER TABLE `orderdetail`
  ADD KEY `FKgcotbta7ruggbobqttkev2cr5` (`orderId`),
  ADD KEY `FK9k7ndeclwpg4ifnofwgnhpv03` (`recipeId`);

--
-- Chỉ mục cho bảng `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKpl79a31yafgam9c0tklym0lbu` (`paymentId`),
  ADD KEY `FKbhieamq65ke02r8ijfso5tivn` (`customerId`);

--
-- Chỉ mục cho bảng `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `recipe`
--
ALTER TABLE `recipe`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `FKbpiwkalim7jvqetryroi8p2ao` (`iceCreamId`),
  ADD KEY `FK483xp15xdsnnb3ykateo4igie` (`userId`);

--
-- Chỉ mục cho bảng `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `userrole`
--
ALTER TABLE `userrole`
  ADD KEY `FKtbick5dbrpnos6ll2175dt5qr` (`user_id`),
  ADD KEY `FKf9a7cojfuvf40x6co16kxa1jb` (`role_id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `customer`
--
ALTER TABLE `customer`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `icecream`
--
ALTER TABLE `icecream`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `payment`
--
ALTER TABLE `payment`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `recipe`
--
ALTER TABLE `recipe`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `role`
--
ALTER TABLE `role`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `feedback`
--
ALTER TABLE `feedback`
  ADD CONSTRAINT `FK13hkc5jl898fls7w1fy1tl6pf` FOREIGN KEY (`orderId`) REFERENCES `orders` (`id`),
  ADD CONSTRAINT `FKnxytts1ey6ykfk9w81ic0ogh8` FOREIGN KEY (`orderId`) REFERENCES `orders` (`id`),
  ADD CONSTRAINT `feedback_ibfk_1` FOREIGN KEY (`orderId`) REFERENCES `orders` (`id`),
  ADD CONSTRAINT `feedback_ibfk_2` FOREIGN KEY (`orderId`) REFERENCES `orders` (`id`);

--
-- Các ràng buộc cho bảng `orderdetail`
--
ALTER TABLE `orderdetail`
  ADD CONSTRAINT `FK9k7ndeclwpg4ifnofwgnhpv03` FOREIGN KEY (`recipeId`) REFERENCES `recipe` (`id`),
  ADD CONSTRAINT `FKgcotbta7ruggbobqttkev2cr5` FOREIGN KEY (`orderId`) REFERENCES `orders` (`id`);

--
-- Các ràng buộc cho bảng `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `FKbhieamq65ke02r8ijfso5tivn` FOREIGN KEY (`customerId`) REFERENCES `customer` (`id`),
  ADD CONSTRAINT `FKpl79a31yafgam9c0tklym0lbu` FOREIGN KEY (`paymentId`) REFERENCES `payment` (`id`),
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`paymentId`) REFERENCES `payment` (`id`);

--
-- Các ràng buộc cho bảng `recipe`
--
ALTER TABLE `recipe`
  ADD CONSTRAINT `FK483xp15xdsnnb3ykateo4igie` FOREIGN KEY (`userId`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `FKbpiwkalim7jvqetryroi8p2ao` FOREIGN KEY (`iceCreamId`) REFERENCES `icecream` (`id`);

--
-- Các ràng buộc cho bảng `userrole`
--
ALTER TABLE `userrole`
  ADD CONSTRAINT `FKf9a7cojfuvf40x6co16kxa1jb` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`),
  ADD CONSTRAINT `FKtbick5dbrpnos6ll2175dt5qr` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
