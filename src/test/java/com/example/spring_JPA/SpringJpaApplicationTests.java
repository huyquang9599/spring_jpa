package com.example.spring_JPA;

import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import com.example.spring_JPA.model.*;
import com.example.spring_JPA.repo.*;

@SpringBootTest
class SpringJpaApplicationTests {
	int i = 0;
	@Autowired
	user_repo userRepo;
	@Autowired
	role_repo roleRepo;
	@Autowired
	recipe_repo recipeRepo;
	@Autowired
	payment_repo paymentRepo;
	@Autowired
	orderdetails_repo orderdetailsRepo;
	@Autowired
	order_repo orderRepo;
	
	@Autowired
	feedback_repo feedbackRepo;
	@Autowired
	faq_repo faqRepo;
	@Autowired
	customer_repo customerRepo;
	Iterable<User> user;

	void incI() {
		this.i++;
	}

	@Test
	void testCountUser() {
		i = 0;
		userRepo.findAll().forEach(e -> {
			incI();
		});
		Assert.assertEquals("findAllUser fail", i, 7);

	}

	@Test
	void testFindByName() {
		String name = "huy2";
		User u = new User();

		u = userRepo.findByName(name);
		Assert.assertEquals("findUserByName fail", u.getName(), name);
	}

	@Test
	void testGetRoleByName() {
		String roleEx = "ROLE_ADMIN";
		
		String roleAc = userRepo.getRoleByUsername("huy");
		Assert.assertEquals("testGetRoleByName fail", roleEx, roleAc);
	}

	@Test
	void testGetList5User() {
		i = 0;
		userRepo.getList5User(0).forEach(e -> {
			incI();
		});
		Assert.assertEquals("testGetList5User fail", 5, i);
	}

	@Test
	void testGetTotalPageUser() {
		Assert.assertEquals("totalPageUser fail", 1, (int) Math.ceil(userRepo.totalPage() / (double) 5) - 1);
	}

	@Test
	void testCountRole() {
		i = 0;
		roleRepo.findAll().forEach(e -> {
			incI();
		});
		Assert.assertEquals("testCountRole fail", 2, i);
	}

	@Test
	void testCountRecipe() {
		i = 0;
		recipeRepo.findAll().forEach(e -> {
			incI();
		});
		Assert.assertEquals(1, i);
	}

	@Test
	void testFindRecipeByUserId() {
		List<Recipe> lr = new ArrayList<Recipe>();
		Long id = (long) 1;
		lr = recipeRepo.findByUserId(id);
		Assert.assertEquals(lr.size(), 1);
	}


	@Test
	void testCountPayment() {
		i = 0;
		paymentRepo.findAll().forEach(e -> {
			incI();
		});
		Assert.assertEquals(1, i);
	}

	@Test
	void testCountOrderDetails() {
		i = 0;
		orderdetailsRepo.findAll().forEach(e -> {
			incI();
		});
		Assert.assertEquals(0, i);
	}

	@Test
	void testCountOrder() {
		i = 0;
		orderRepo.findAll().forEach(e -> {
			incI();
		});
		Assert.assertEquals(0, i);
	}

	@Test
	void testFindByCustomerId() {
		List<Orders> lo = new ArrayList<Orders>();
		Long id = (long) 2;
		lo = orderRepo.findByCustomerId(id);
		Assert.assertEquals(lo.size(), 0);
	}

	
	@Test
	void testCountFeedback() {
		i = 0;
		feedbackRepo.findAll().forEach(e -> {
			incI();
		});
		Assert.assertEquals(0, i);
	}


	@Test
	void testCountFaq() {
		i = 0;
		faqRepo.findAll().forEach(e -> {
			incI();
		});
		Assert.assertEquals(5, i);
	}

	@Test
	void testCountCustomer() {
		i = 0;
		customerRepo.findAll().forEach(e -> {
			incI();
		});
		Assert.assertEquals(1, i);
	}

	@Test
	void testFindCustomerByName() {
		String nameEx = "huy";
		Customer c = customerRepo.findByName(nameEx);
		String nameAc = c.getName();
		Assert.assertEquals(nameEx, nameAc);
	}

	@Test
	void testGetList5Customer() {
		i = 0;
		customerRepo.getList5Customer(0).forEach(e -> {
			incI();
		});
		Assert.assertEquals(1, i);
	}

	@Test
	void testGetTotalPageCustomer() {
		Assert.assertEquals(0, (int) Math.ceil(customerRepo.totalPage() / (double) 5) - 1);
	}
}
