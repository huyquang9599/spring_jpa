package com.example.spring_JPA.controller;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder.BCryptVersion;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import com.example.spring_JPA.JWT.CustomUserDetails;
import com.example.spring_JPA.JWT.JwtRequest;
import com.example.spring_JPA.JWT.JwtResponse;
import com.example.spring_JPA.JWT.JwtTokenUtil;

import com.example.spring_JPA.model.*;
import com.example.spring_JPA.repo.customer_repo;
import com.example.spring_JPA.repo.faq_repo;
import com.example.spring_JPA.repo.feedback_repo;
import com.example.spring_JPA.repo.icecream_repo;

import com.example.spring_JPA.repo.order_repo;
import com.example.spring_JPA.repo.orderdetails_repo;
import com.example.spring_JPA.repo.payment_repo;
import com.example.spring_JPA.repo.recipe_repo;
import com.example.spring_JPA.repo.role_repo;
import com.example.spring_JPA.repo.user_repo;

@Controller
@CrossOrigin
@RequestMapping(path = "/icecream")
public class MainController {
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

//	"username":"huy2",
//	"password":"mypass3"
	private BCryptPasswordEncoder bcryptEncoder = new BCryptPasswordEncoder(BCryptVersion.$2A, 31);

	@PostMapping(value = "/login")
	public ResponseEntity<?> authenticateUser(@RequestBody JwtRequest req) {
		Authentication authentication = authenticationManager
				.authenticate(new UsernamePasswordAuthenticationToken(req.getUsername(), req.getPassword()

				));
		SecurityContextHolder.getContext().setAuthentication(authentication);
		String token = jwtTokenUtil.generateToken((CustomUserDetails) authentication.getPrincipal());

		
		return ResponseEntity.ok(new JwtResponse(token));

	}
	
	
	
////////////////////////	USER	////////////////////////

	@Autowired
	private user_repo userRepo;
	
	@GetMapping(path = "/alluser")
	public @ResponseBody Iterable<User> getAllUsers() {
		return userRepo.findAll();
	}
	
	@GetMapping(path = "/getuserimagebyid/{id}")
	public @ResponseBody Optional<byte[]> getuserimagebyid(@PathVariable Long id) throws IOException {
		return userRepo.getImageById(id);
	}
	@PostMapping("/uploaduserimagebyid/{id}")
	public @ResponseBody void uploadUserImageById(@RequestParam("myFile") MultipartFile file,@PathVariable Long id) throws IOException {
		userRepo.setAvatar(file.getBytes(), id);
		//System.out.println("Image saved");
	}
	

	@PostMapping(path = "/updateuser")
	public @ResponseBody User updateUser(@RequestBody User user) {
		return userRepo.save(user);
	}

	@PostMapping(path = "/adduser")
	public @ResponseBody ResponseEntity<?> addUser(@RequestBody User user) {
		User newUser = new User();
		newUser.setName(user.getName());
		newUser.setPassword(bcryptEncoder.encode(user.getPassword()).toString());
		userRepo.save(newUser);
		Long roleid = roleRepo.getIdByRole("ROLE_USER");
		Long userid = userRepo.getIdByUsername(newUser.getName());
		userRepo.addRoleForUser(roleid, userid);
		return ResponseEntity.ok(newUser);
	}

	@GetMapping(path = "/totaluserpage")
	public @ResponseBody Integer getTotalUserPage() {
		return (int) Math.ceil(userRepo.totalPage() / (double) 5) - 1;
	}

	@GetMapping(path = "/get5user/{page}")
	public @ResponseBody Iterable<User> getList5Users(@PathVariable Integer page) {
		return userRepo.getList5User(page * 5);
	}

	@GetMapping("/finduserbyid/{id}")
	public @ResponseBody Optional<User> finduserbyid(@PathVariable Long id) {
		return userRepo.findById(id);
	}

	@GetMapping("/finduserbyname/{name}")
	public @ResponseBody User finduserbyid(@PathVariable String name) {
		return userRepo.findByName(name);
	}

	@GetMapping("/deleteuserbyid/{id}")
	public @ResponseBody void deletebyid(@PathVariable Long id) {
		userRepo.deleteRoleForUser(id);
		userRepo.deleteById(id);

	}
////////////////////////	  PAYMENT		////////////////////////

	@Autowired
	private payment_repo payRepo;

	@PreAuthorize("hasRole('')")
	@GetMapping(path = "/allpayment")
	public @ResponseBody Iterable<Payment> getAllPayments() {
		return payRepo.findAll();
	}

	@GetMapping("/findpaymentbyid/{id}")
	public @ResponseBody Optional<Payment> findpaymentbyid(@PathVariable Long id) {
		return payRepo.findById(id);
	}

////////////////////////	CUSTOMER	////////////////////////

	@Autowired
	private customer_repo cusRepo;

	@PostMapping(value = "/loginfail/{username}")
	public @ResponseBody void loginFail(@PathVariable String username) throws IOException {
		cusRepo.increaseLoginFail(username);
	}

	@GetMapping(path = "/allcustomer")
	public @ResponseBody Iterable<Customer> getAllCustomers() {
		return cusRepo.findAll();
	}
	
	@GetMapping(path = "/getcusimagebyid/{id}")
	public @ResponseBody Optional<byte[]> getcusimagebyid(@PathVariable Long id) throws IOException {
		return cusRepo.getImageById(id);
	}
	@PostMapping("/uploadcusimagebyid/{id}")
	public @ResponseBody void uploadcusImageById(@RequestParam("myFile") MultipartFile file,@PathVariable Long id) throws IOException {
		cusRepo.setAvatar(file.getBytes(), id);
		//System.out.println("Image saved");
	}

	@GetMapping(path = "/totalcustomerpage")
	public @ResponseBody Integer getTotalCustomerPage() {
		return (int) Math.ceil(cusRepo.totalPage() / (double) 5) - 1;
	}

	@PostMapping(path = "/updatecustomer")
	public @ResponseBody Customer updateCustomer(@RequestBody Customer customer) {
		
		return cusRepo.save(customer);
	}

	@GetMapping(path = "/get5customer/{page}")
	public @ResponseBody Iterable<Customer> getList5Customers(@PathVariable Integer page) {
		return cusRepo.getList5Customer(page * 5);
	}

	@GetMapping("/findcustomerbyid/{id}")
	public @ResponseBody Optional<Customer> findcustomerbyid(@PathVariable Long id) {
		return cusRepo.findById(id);
	}

	@GetMapping("/findcustomerbyname/{name}")
	public @ResponseBody Customer findcustomerbyname(@PathVariable String name) {
		return cusRepo.findByName(name);
	}

	@GetMapping("/deletecustomerbyid/{id}")
	public @ResponseBody void deletecustomerbyid(@PathVariable Long id) {
		cusRepo.deleteById(id);
	}

//	@GetMapping("/deletecustomerbyname/{name}")
//	public @ResponseBody void deletecustomerbyname(@PathVariable String name) {
//		cusRepo.deleteByName(name);
//	}

////////////////////////	FAQ		////////////////////////

	@Autowired
	private faq_repo faqRepo;

	@GetMapping(path = "/allfaq")
	public @ResponseBody Iterable<Faq> getAllFaqs() {
		return faqRepo.findAll();
	}

	@GetMapping("/findfaqbyid/{id}")
	public @ResponseBody Optional<Faq> findfaqbyid(@PathVariable Long id) {
		return faqRepo.findById(id);
	}

	@GetMapping("/deletefaqbyid/{id}")
	public @ResponseBody void deletefaqbyid(@PathVariable Long id) {
		faqRepo.deleteById(id);
	}

////////////////////////	FEEDBACK	////////////////////////

	@Autowired
	private feedback_repo feedbackRepo;

	@GetMapping(path = "/allfeedback")
	public @ResponseBody Iterable<Feedback> getAllFeedbacks() {
		return feedbackRepo.findAll();
	}

	@GetMapping("/findfeedbackbyid/{id}")
	public @ResponseBody Optional<Feedback> findfeedbackbyid(@PathVariable Long id) {
		return feedbackRepo.findById(id);
	}
	
	@GetMapping("/getfeedbackbyrecipeid/{id}")
	public @ResponseBody String[] getfeedbackbyrecipeid(@PathVariable Long id) {
		return feedbackRepo.getFeedbackByRecipeId(id);
	}

////////////////////////	ORDERS	////////////////////////
	@Autowired
	private order_repo orderRepo;

	@GetMapping(path = "/allorder")
	public @ResponseBody Iterable<Orders> getAllOrders() {
		return orderRepo.findAll();
	}

	@GetMapping(path = "/totalorderspage")
	public @ResponseBody Integer getTotalOrdersPage() {
		return (int) Math.ceil(orderRepo.totalPage() / (double) 5) - 1;
	}

	@PostMapping(path = "/addorders")
	public @ResponseBody Orders addOrders(@RequestBody Orders orders) {
		return orderRepo.save(orders);
	}

	
	@GetMapping(path = "/updateordersstatusbyid/{id}/{status}")
	public @ResponseBody Optional<Orders> updateOrders(@PathVariable("id") Long id, @PathVariable("status") Integer status) {
		Orders r = orderRepo.findOrdersById(id);
		r.setStatus(status);
		orderRepo.save(r);
		return Optional.of(r);
	}

	@GetMapping(path = "/get5orders/{page}")
	public @ResponseBody Iterable<Orders> getList5Orders(@PathVariable Integer page) {
		return orderRepo.getList5Orders(page * 5);
	}

	@GetMapping("/findorderbyid/{id}")
	public @ResponseBody Optional<Orders> findorderbyid(@PathVariable Long id) {
		return orderRepo.findById(id);
	}

	@GetMapping("/findorderbycustomerid/{customerid}/{page}")
	public @ResponseBody Iterable<Orders>findorderbycustomerid(@PathVariable Long customerid,@PathVariable Integer page) {
		return orderRepo.findByCustomerId(customerid,page * 5);
	}

	@GetMapping("/deleteordersbyid/{id}")
	public @ResponseBody void deleteordersbyid(@PathVariable Long id) {
		orderRepo.deleteById(id);
	}
	//////////////////////// ROLE ////////////////////////

	@Autowired
	private role_repo roleRepo;

	@GetMapping(path = "/allrole")
	public @ResponseBody Iterable<Role> getAllRoles() {
		return roleRepo.findAll();
	}

	@GetMapping("/findrolebyid/{id}")
	public @ResponseBody Optional<Role> findrolebyid(@PathVariable Long id) {
		return roleRepo.findById(id);
	}

	@RequestMapping(value = "/addrole", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody Role addProduct(@Valid @RequestBody Role role) {
		roleRepo.save(role);
		return role;
	}

	@GetMapping("/deleterolebyid/{id}")
	public @ResponseBody void deleterolebyid(@PathVariable Long id) {
		roleRepo.deleteById(id);
	}

////////////////////////	RECIPE	////////////////////////

	@Autowired
	private recipe_repo recipeRepo;

	@GetMapping(path = "/allrecipe")
	public @ResponseBody Iterable<Recipe> getAllRecipes() {
		return recipeRepo.findAll();
	}

	@GetMapping(path = "/getrecipeimagebyid/{id}")
	public @ResponseBody Optional<byte[]> getrecipeimagebyid(@PathVariable Long id) throws IOException {
		return recipeRepo.getImageById(id);
	}
	@PostMapping("/uploadrecipeimagebyid/{id}")
	public @ResponseBody void uploadRecipeImageById(@RequestParam("myFile") MultipartFile file,@PathVariable Long id) throws IOException {
		recipeRepo.setImage(file.getBytes(), id);
		//System.out.println("Image saved");
	}

	@GetMapping("/increaserecipeviewcount/{id}")
	public @ResponseBody void increaserecipeviewcount(@PathVariable Long id) {
		recipeRepo.increaseViewCount(id);
		
	}
	
	@PostMapping(path = "/updaterecipe")
	public @ResponseBody Recipe updateRecipe(@RequestBody Recipe recipe) {
		return recipeRepo.save(recipe);
	}

	@GetMapping(path = "/totalrecipepage")
	public @ResponseBody Integer getTotalRecipePage() {
		return (int) Math.ceil(recipeRepo.totalPage() / (double) 5) - 1;
	}

	@GetMapping(path = "/get5recipe/{page}")
	public @ResponseBody Iterable<Recipe> getList5Recipe(@PathVariable Integer page) {
		return recipeRepo.getList5Recipe(page * 5);
	}

	@GetMapping("/findrecipebyid/{id}")
	public @ResponseBody Optional<Recipe> findrecipebyid(@PathVariable Long id) {
		return recipeRepo.findById(id);
	}

	@GetMapping("/findrecipebyuserid/{userid}")
	public @ResponseBody List<Recipe> findrecipebyuserid(@PathVariable Long userid) {
		return recipeRepo.findByUserId(userid);
	}

	
	@GetMapping("/deleterecipebyid/{id}")
	public @ResponseBody void deleterecipebyid(@PathVariable Long id) {
		recipeRepo.deleteById(id);
	}

////////////////////////	ORDERDETAILS	////////////////////////

	@Autowired
	private orderdetails_repo orderdetailsRepo;

	@GetMapping(path = "/allorderdetails")
	public @ResponseBody Iterable<Orderdetails> getAllOrderdetails() {
		return orderdetailsRepo.findAll();
	}

	@GetMapping("/findorderdetailsbyid/{id}")
	public @ResponseBody Iterable<Orderdetails> findorderdetailsbyid(@PathVariable Long id) {
		return orderdetailsRepo.getOrdersDetailsById(id);
	}
	
	
	@PostMapping(path = "/addorderdetails")
	public @ResponseBody void addOrders(@RequestBody Orderdetails orderdetails) {
		 orderdetailsRepo.addOrderDetail(orderdetails.getRecipeid(), orderdetails.getQuantity(), orderdetails.getPrice());
	}
	
	
}