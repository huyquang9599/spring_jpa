package com.example.spring_JPA.JWT;

import java.util.Collection;
import java.util.Collections;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.example.spring_JPA.model.User;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CustomUserDetails implements UserDetails {
	String username;
	String password;
	Long id;
	private static final long serialVersionUID = 3206442206864534754L;
	String authority;
	
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
    
    		    return Collections.singleton(new SimpleGrantedAuthority(authority));
    }

	public CustomUserDetails(Long id,String username,String password, String authority) {
		super();
		this.id=id;
		this.username=username;
		this.password=password;
		this.authority = authority ;
	}

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}