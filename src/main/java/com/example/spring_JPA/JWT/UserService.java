package com.example.spring_JPA.JWT;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import com.example.spring_JPA.model.*;
import com.example.spring_JPA.repo.customer_repo;
import com.example.spring_JPA.repo.user_repo;

@Service
public class UserService implements UserDetailsService {

	@Autowired
	private user_repo userRepo;
	
	@Autowired
	private customer_repo cusRepo;

	@Override
	public CustomUserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepo.findByName(username);
		Customer cus = cusRepo.findByName(username);
		String role;
		if(cus!=null) {
			role = "ROLE_CUSTOMER";
			return new CustomUserDetails(cus.getId(),cus.getName(),cus.getPassword(), role);
		}else if(user == null) {
			throw new UsernameNotFoundException("Unknown user:"+username);
		}else {
			role = userRepo.getRoleByUsername(username);
			return new CustomUserDetails(user.getId(),user.getName(),user.getPassword(), role);
		}
		

		
		// System.out.println(role);
		
	}

}