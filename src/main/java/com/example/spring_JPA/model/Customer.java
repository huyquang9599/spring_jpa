package com.example.spring_JPA.model;

import java.util.Collection;

import javax.persistence.*;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@Entity
@Table(name = "customer")

public class Customer {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "email")
	private String email;

	@Column(name = "phone")
	private String phone;

	@Column(name = "password", nullable = false)
	private String password;

	@Column(name = "DOB")
	private String DOB;

	@Column(name = "address")
	private String address;

	@Column(name = "gender")
	private boolean gender;

	@Lob
	@Column(name = "avatar")
	private byte[] avatar;

	@Column(name = "status")
	private boolean status;

	@Column(name = "numofloginfailed")
	private Integer numofloginfailed;

	@OneToMany(mappedBy = "customer", cascade = CascadeType.ALL)
	@EqualsAndHashCode.Exclude
	@ToString.Exclude
	private Collection<Orders> orders;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDOB() {
		return DOB;
	}

	public void setDOB(String dOB) {
		DOB = dOB;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public boolean isGender() {
		return gender;
	}

	public void setGender(boolean gender) {
		this.gender = gender;
	}

	

	public byte[] getAvatar() {
		return avatar;
	}

	public void setAvatar(byte[] avatar) {
		this.avatar = avatar;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public Integer getNumofloginfailed() {
		return numofloginfailed;
	}

	public void setNumofloginfailed(Integer numofloginfailed) {
		this.numofloginfailed = numofloginfailed;
	}

}
