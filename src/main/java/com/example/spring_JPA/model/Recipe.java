package com.example.spring_JPA.model;

import java.util.Collection;

import javax.persistence.*;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@Entity
@Table(name = "recipe")
public class Recipe {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private Long id;

	@Column(name = "userid", nullable = false)
	private Long userid;


	@Column(name = "title")
	private String title;

	@Column(name = "description")
	private String description;

	@Column(name = "price")
	private double price;

	@Column(name = "status")
	private boolean status;

	@Column(name = "viewcount")
	private Integer viewcount;

	@Lob
	@Column(name = "image")
	private byte[] image;

	@Column(name = "uploaddate")
	private String uploaddate;

	@OneToMany(mappedBy = "recipe", cascade = CascadeType.ALL)
	@EqualsAndHashCode.Exclude
	@ToString.Exclude
	private Collection<Feedback> feedback;
	
	@ManyToOne
	@JoinColumn(name = "userid", insertable = false, updatable = false)
	@EqualsAndHashCode.Exclude
	@ToString.Exclude
	private User user;

	@OneToMany(mappedBy = "recipe", cascade = CascadeType.ALL)
	@EqualsAndHashCode.Exclude
	@ToString.Exclude
	private Collection<Orderdetails> orderdetails;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserid() {
		return userid;
	}

	public void setUserid(Long userid) {
		this.userid = userid;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public Integer getViewcount() {
		return viewcount;
	}

	public void setViewcount(Integer viewcount) {
		this.viewcount = viewcount;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public String getUploaddate() {
		return uploaddate;
	}

	public void setUploaddate(String uploaddate) {
		this.uploaddate = uploaddate;
	}

}
