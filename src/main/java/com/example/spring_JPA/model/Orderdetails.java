package com.example.spring_JPA.model;

import javax.persistence.*;

import lombok.*;

@Entity
@Table(name = "orderdetail")
public class Orderdetails {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private Long id;
	
	@Column(name = "orderid", nullable = false)
	private Long orderid;

	@Column(name = "recipeid", nullable = false)
	private Long recipeid;

	@Column(name = "quantity")
	private Integer quantity;

	@Column(name = "price")
	private double price;

	@Column(name = "notes")
	private String notes;

	@ManyToOne
	@JoinColumn(name = "orderid", insertable = false, updatable = false)
	@EqualsAndHashCode.Exclude
	@ToString.Exclude
	private Orders orders;

	@ManyToOne
	@JoinColumn(name = "recipeid", insertable = false, updatable = false)
	@EqualsAndHashCode.Exclude
	@ToString.Exclude
	private Recipe recipe;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getOrderid() {
		return orderid;
	}

	public void setOrderid(Long orderid) {
		this.orderid = orderid;
	}

	public Long getRecipeid() {
		return recipeid;
	}

	public void setRecipeid(Long recipeid) {
		this.recipeid = recipeid;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

}
