package com.example.spring_JPA.model;

import java.util.Collection;

import javax.persistence.*;

import lombok.*;

@Entity
@Table(name = "orders")
public class Orders {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private Long id;

	@Column(name = "customerid", nullable = false)
	private Long customerid;

	@Column(name = "paymentoption")
	private String paymentoption;

	@Column(name = "paymentid", nullable = false)
	private Long paymentid;

	@Column(name = "createdate")
	private String createdate;

	@Column(name = "deliverydetail")
	private String deliverydetail;

	@Column(name = "notes")
	private String notes;

	@Column(name = "status")
	private Integer status;

	

	@OneToMany(mappedBy = "orders", cascade = CascadeType.ALL)
	@EqualsAndHashCode.Exclude
	@ToString.Exclude
	private Collection<Orderdetails> orderdetails;

	@ManyToOne
	@JoinColumn(name = "paymentid", insertable = false, updatable = false)
	@EqualsAndHashCode.Exclude
	@ToString.Exclude
	private Payment payment;

	@ManyToOne
	@JoinColumn(name = "customerid", insertable = false, updatable = false)
	@EqualsAndHashCode.Exclude
	@ToString.Exclude
	private Customer customer;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCustomerid() {
		return customerid;
	}

	public void setCustomerid(Long customerid) {
		this.customerid = customerid;
	}

	public String getPaymentoption() {
		return paymentoption;
	}

	public void setPaymentoption(String paymentoption) {
		this.paymentoption = paymentoption;
	}

	public Long getPaymentid() {
		return paymentid;
	}

	public void setPaymentid(Long paymentid) {
		this.paymentid = paymentid;
	}

	public String getCreatedate() {
		return createdate;
	}

	public void setCreatedate(String createdate) {
		this.createdate = createdate;
	}

	public String getDeliverydetail() {
		return deliverydetail;
	}

	public void setDeliverydetail(String deliverydetail) {
		this.deliverydetail = deliverydetail;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	

}
