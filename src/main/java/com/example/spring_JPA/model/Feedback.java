package com.example.spring_JPA.model;

import javax.persistence.*;

import lombok.*;

@Entity
@Table(name = "fedback")

public class Feedback {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private Long id;

	@Column(name = "recipeid", nullable = false)
	private Long recipeid;

	@Column(name = "details")
	private String details;

	@Column(name = "createddate")
	private String createddate;

	@ManyToOne
	@JoinColumn(name = "recipeid", insertable = false, updatable = false)
	@EqualsAndHashCode.Exclude
	@ToString.Exclude
	private Recipe recipe;

	

	public Long getRecipeid() {
		return recipeid;
	}

	public void setRecipeid(Long recipeid) {
		this.recipeid = recipeid;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public String getCreateddate() {
		return createddate;
	}

	public void setCreateddate(String createddate) {
		this.createddate = createddate;
	}

}