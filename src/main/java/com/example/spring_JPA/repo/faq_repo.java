package com.example.spring_JPA.repo;

import org.springframework.data.repository.CrudRepository;

import com.example.spring_JPA.model.Faq;

public interface faq_repo extends CrudRepository<Faq, Long>{
	
}
