package com.example.spring_JPA.repo;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.example.spring_JPA.model.Customer;

public interface customer_repo extends CrudRepository<Customer, Long> {
	@Query("SELECT e FROM Customer e WHERE e.name = ?1")
	Customer findByName(String name);

	@Query(value = "select * from customer limit 5 offset ?1", nativeQuery = true)
	Iterable<Customer> getList5Customer(Integer page);

	@Query(value = "select Count(*) from customer", nativeQuery = true)
	Integer totalPage();

	@Query(value = "SELECT avatar FROM customer  WHERE id = ?1", nativeQuery = true)
	Optional<byte[]> getImageById(Long id);
	
	@Query(value = "select Count(*) from customer where name =?1", nativeQuery = true)
	Integer checkExist(String name);
	
	
	@Transactional
	@Modifying
	@Query(value = "Update customer set numofloginfailed = numofloginfailed+1 where name = ?1", nativeQuery = true)
	void increaseLoginFail(String username);
	
	@Transactional
	@Modifying
	@Query(value = "select id from user where name=?1", nativeQuery = true)
	void getIdByUsername(String username);
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE `customer` SET `avatar`=?1 WHERE id = ?2",nativeQuery=true)
	void setAvatar(byte[] avatar,Long id);
}
