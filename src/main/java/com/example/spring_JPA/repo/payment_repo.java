package com.example.spring_JPA.repo;

import org.springframework.data.repository.CrudRepository;

import com.example.spring_JPA.model.*;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface payment_repo extends CrudRepository<Payment, Long> {

}