package com.example.spring_JPA.repo;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.example.spring_JPA.model.*;

public interface orderdetails_repo extends CrudRepository<Orderdetails, Long> {
	@Query(value = "select * from orderdetail where orderid=?1", nativeQuery = true)
	Iterable<Orderdetails> getOrdersDetailsById(Long id);
	
	
	@Transactional
	@Modifying
	@Query(value = "INSERT INTO `orderdetail`(`orderId`, `recipeId`, `quantity`, `price`) VALUES ((SELECT id FROM `orders` group by id DESC limit 1),?1,?2,?3)", nativeQuery = true)
	void addOrderDetail(Long recipeid,Integer quantity,double price);
}