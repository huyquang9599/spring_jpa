package com.example.spring_JPA.repo;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.example.spring_JPA.model.*;

public interface role_repo extends CrudRepository<Role, Long> {
	@Query(value = "select id from Role where role=?1", nativeQuery = true)
	Long getIdByRole(String role);
}