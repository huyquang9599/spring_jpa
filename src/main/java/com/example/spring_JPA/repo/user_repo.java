package com.example.spring_JPA.repo;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.spring_JPA.model.*;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete
@Repository
public interface user_repo extends CrudRepository<User, Long> {
	@Query(value = "SELECT distinct * FROM User  WHERE name like ?1", nativeQuery = true)
	User findByName(String name);

	@Query(value = "select get_role_by_username(?1)", nativeQuery = true)
	String getRoleByUsername(String name);

	@Query(value = "select * from user limit 5 offset ?1", nativeQuery = true)
	Iterable<User> getList5User(Integer page);

	@Query(value = "select Count(*) from user", nativeQuery = true)
	Integer totalPage();

	@Query(value = "SELECT * FROM User  WHERE id = ?1", nativeQuery = true)
	User findUserById(Long id);

	@Query(value = "SELECT avatar FROM User  WHERE id = ?1", nativeQuery = true)
	Optional<byte[]> getImageById(Long id);
	
	@Query(value = "select Count(*) from user where name =?1", nativeQuery = true)
	Integer checkExist(String name);
	
	@Transactional
	@Modifying
	@Query(value = "INSERT INTO `userrole`(`role_id`, `user_id`) VALUES (?1,?2)", nativeQuery = true)
	void addRoleForUser(Long role_id, Long user_id);

	@Transactional
	@Modifying
	@Query(value = "UPDATE `user` SET `avatar`=?1 WHERE id = ?2",nativeQuery=true)
	void setAvatar(byte[] avatar,Long id);
	
	@Transactional
	@Modifying
	@Query(value = "DELETE FROM `userrole` WHERE user_id=?1", nativeQuery = true)
	void deleteRoleForUser(Long userid);

	@Query(value = "select id from user where name=?1", nativeQuery = true)
	Long getIdByUsername(String username);
	
	
//	@Query(value = "select isnameexist(?1)",nativeQuery = true)
//	boolean checkExistName(String name);
}