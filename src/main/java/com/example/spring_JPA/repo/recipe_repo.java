package com.example.spring_JPA.repo;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.example.spring_JPA.model.*;

public interface recipe_repo extends CrudRepository<Recipe, Long> {
	@Query("select e from Recipe e where e.userid= ?1")
	List<Recipe> findByUserId(Long userid);

	@Query(value = "SELECT * FROM Recipe  WHERE id = ?1", nativeQuery = true)
	Recipe findRecipeById(Long id);

	@Query(value = "select * from Recipe limit 5 offset ?1", nativeQuery = true)
	Iterable<Recipe> getList5Recipe(Integer page);

	@Query(value = "select Count(*) from recipe", nativeQuery = true)
	Integer totalPage();

	@Query(value = "SELECT image FROM recipe  WHERE id = ?1", nativeQuery = true)
	Optional<byte[]> getImageById(Long id);

	@Transactional
	@Modifying
	@Query(value = "UPDATE `recipe` SET `image`=?1 WHERE id = ?2", nativeQuery = true)
	void setImage(byte[] image, Long id);
	
	@Transactional
	@Modifying
	@Query(value = "Update recipe set viewcount = viewcount+1 where id = ?1", nativeQuery = true)
	void increaseViewCount(Long id);
}