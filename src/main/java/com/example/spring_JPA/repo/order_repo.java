package com.example.spring_JPA.repo;



import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.spring_JPA.model.*;
@Repository
public interface order_repo extends CrudRepository<Orders, Long>{
	@Query(value="select * from Orders  where customerid= ?1 order by id desc limit 5 offset ?2", nativeQuery = true)
	Iterable<Orders> findByCustomerId(Long customerid,Integer page);
	
	@Query(value = "select * from orders limit 5 offset ?1", nativeQuery = true)
	Iterable<Orders> getList5Orders(Integer page);
	
	@Query(value = "select Count(*) from orders", nativeQuery = true)
	Integer totalPage();

	@Query(value = "SELECT * FROM Orders  WHERE id = ?1", nativeQuery = true)
	Orders findOrdersById(Long id);
}