package com.example.spring_JPA.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.example.spring_JPA.model.Feedback;
import com.example.spring_JPA.model.Orderdetails;;

public interface feedback_repo extends CrudRepository<Feedback, Long> {
	@Query(value = "select details from fedback where recipeid=?1", nativeQuery = true)
	String[] getFeedbackByRecipeId(Long id);
}